package com.wavelabs.ai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class AshynchronousApplication extends SpringBootServletInitializer implements CommandLineRunner {

	public static void main(String[] args)  throws Exception    {
		SpringApplication.run(AshynchronousApplication.class, args);
	}
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(SpringBootWebApplication.class);
	    }
	    @Override
	    public void run(String... args) throws Exception {
	        logger.info("Application Started !!");
	    }
	}
}
