package com.wavelabs.ai.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class SampleController {

	@Autowired
	RestTemplate restTemplate;
	
	@Value("${server.port}")
	private int portNumber;
	
	

	@RequestMapping("/test")
	public ResponseEntity<Map> getMap() {
		ResponseEntity<Map> response = restTemplate.getForEntity("http://School/port", Map.class);
		return response;
	}
	
	@RequestMapping("/myport")
	public ResponseEntity<Map> getMyPOrt() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("port", String.valueOf(portNumber));
		return ResponseEntity.status(200).body(map);
	}
	
	
	
}
