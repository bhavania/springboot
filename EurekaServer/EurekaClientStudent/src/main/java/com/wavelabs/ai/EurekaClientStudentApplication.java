package com.wavelabs.ai;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name="School")
public class EurekaClientStudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaClientStudentApplication.class, args);
	}
	@Bean
	@LoadBalanced
	public RestTemplate restTempalte() {
		
		return new RestTemplate();
	}
}