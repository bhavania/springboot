package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;
@Service
@SpringBootApplication
public class MyXMLApplication {

			private MessageService service;
			@Autowired
			public void setService(MessageService svc){
				this.service=svc;
			}

			public boolean processMessage(String msg, String rec) {
				return this.service.sendMessage(msg, rec);
			}
		}
