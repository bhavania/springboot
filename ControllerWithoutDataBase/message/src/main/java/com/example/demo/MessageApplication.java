package com.example.demo;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessageApplication {

	public static void main(String[] args) {
		 ApplicationContext context=SpringApplication.run(MessageApplication.class, args);
		 MyXMLApplication obj=context.getBean(MyXMLApplication.class);
		boolean res= obj.processMessage("hi this is bhavani ","anitha");	
		System.out.println(res);
	}
}