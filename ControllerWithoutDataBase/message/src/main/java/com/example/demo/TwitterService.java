package com.example.demo;

import org.springframework.stereotype.Service;
	@Service
	public class TwitterService implements MessageService {

		public boolean sendMessage(String msg, String rec) {
			System.out.println("Twitter message Sent to " + rec + " with Message=" + msg);
			return true;
		}

	}