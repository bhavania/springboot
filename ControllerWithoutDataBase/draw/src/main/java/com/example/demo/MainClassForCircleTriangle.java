package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@Component
public class MainClassForCircleTriangle {
	
	@Autowired
	public  Drawing drw;

	
	public void setDrawing(Drawing drw) {
		this.drw = drw;
	}

	public void designProperty() {
		drw.DrawDesign();
	}
}
