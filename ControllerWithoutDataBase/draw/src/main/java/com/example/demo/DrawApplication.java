package com.example.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrawApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(DrawApplication.class, args);
		MainClassForCircleTriangle obj = context.getBean(MainClassForCircleTriangle.class);
		obj.designProperty();

	}

}
