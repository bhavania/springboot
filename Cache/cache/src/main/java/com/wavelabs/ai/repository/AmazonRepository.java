package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Amazon;
@Repository
public interface AmazonRepository extends JpaRepository<Amazon, Integer> {

}
