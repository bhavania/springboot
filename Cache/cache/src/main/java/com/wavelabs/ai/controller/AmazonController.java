package com.wavelabs.ai.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Amazon;
import com.wavelabs.ai.service.AmazonService;

@RestController
public class AmazonController {
	@Autowired
	private AmazonService amazonService;

	@GetMapping(value = "/amazons")
	public ResponseEntity<List<Amazon>> getAmazonDetails() {
		List<Amazon> Amazon = amazonService.getAmazonDetails();

		return ResponseEntity.status(200).body(Amazon);
	}

	@PostMapping(value = "/amazons")
	public ResponseEntity<Amazon> saveAmazon(@RequestBody Amazon Amazon) {
		Amazon amazon = amazonService.saveAmazon(Amazon);
		return ResponseEntity.status(200).body(amazon);
	}

	@PutMapping(value = "/amazons/{id}")
	public ResponseEntity<Amazon> updateTicket(@PathVariable("id") Integer id, @RequestBody Amazon amazon) {
		amazon.setId(id);
		Amazon amazon1 = amazonService.updateAmazon(amazon);
		return ResponseEntity.status(200).body(amazon1);
	}

}
