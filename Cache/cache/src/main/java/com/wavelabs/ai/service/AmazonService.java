package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Amazon;
import com.wavelabs.ai.repository.AmazonRepository;

@Service
public class AmazonService {
	@Autowired
	private AmazonRepository amazonRepository;

	public Amazon saveAmazon(Amazon amazon) {
		return amazonRepository.save(amazon);
	}

	@Cacheable(value = "amazons")
	public List<Amazon> getAmazonDetails() {
		try {
			System.out.println("Going to sleep for 5 Secs.. to simulate backend call.");
			Thread.sleep(100
					* 5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return amazonRepository.findAll();
	}

	@CachePut(value = "amazons",key="#amazon.id")
	public Amazon updateAmazon(Amazon amazon) {
		return amazonRepository.saveAndFlush(amazon);
	}
}