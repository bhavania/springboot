package com.wavelabs.ai.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.TravellerDetails;
import com.wavelabs.ai.service.TravellerService;

@RestController
public class TravellerController {
@Autowired
private TravellerService travellerService;


@GetMapping(value = "/travellerDetails")
public ResponseEntity<List<TravellerDetails>> GetAllTravellerDetails() {
	List<TravellerDetails> travellerDetails = travellerService.getTravellerDetails();
	return ResponseEntity.status(200).body(travellerDetails);
}
@PostMapping(value = "/travellerDetails")
public ResponseEntity<TravellerDetails> saveTravellerDetails( @RequestBody TravellerDetails travellerDetails) {
	TravellerDetails travellerDetails1 = travellerService.saveTravellerDetails(travellerDetails);
	return ResponseEntity.status(200).body(travellerDetails1);
}
/*@DeleteMapping(value="/tickets/{id}")
public ResponseEntity<Ticket> updateTickets(@PathVariable("id") Integer id,@RequestBody Ticket ticket)
{
Ticket tick = ticketservice.updateTicket(id,ticket);
return ResponseEntity.status(200).body(tick);
}*/

@PutMapping(value="/travellerDetails/{travelid}")
public ResponseEntity<TravellerDetails> updateTravellerDetails(@Valid @PathVariable("travelid") Integer travelid, @RequestBody TravellerDetails travellerDetails)
{
	travellerDetails.setTravelid(travelid);
	TravellerDetails TravellerDetails1=	travellerService.updateTravellerDetails(travellerDetails);
	return ResponseEntity.status(200).body(TravellerDetails1);
	}

}
