package com.wavelabs.ai.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.TravellerDetails;
import com.wavelabs.ai.repository.TravellerRepository;

@Service
public class TravellerService {
	@Autowired
	private TravellerRepository travellerRepository;

	public TravellerDetails saveTravellerDetails(TravellerDetails travellerDetails) {
		return travellerRepository.save(travellerDetails);

	}
	
	//@Scheduled(fixedRate = 5000)
	@Scheduled(cron = "* * * ? * *")
	public List<TravellerDetails> getTravellerDetails() {
		System.out.println("displayed time:" + Calendar.getInstance().getTime());
		return travellerRepository.findAll();
	}

	@Async
	public TravellerDetails updateTravellerDetails(TravellerDetails travellerDetails) {
		System.out.println("method Started");
		try {
			Thread.sleep(5000);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("method complted");
		return travellerRepository.saveAndFlush(travellerDetails);

	}
}
