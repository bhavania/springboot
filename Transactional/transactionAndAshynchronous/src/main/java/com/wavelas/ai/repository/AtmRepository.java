package com.wavelas.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelas.ai.model.Account;
@Repository
public interface AtmRepository extends JpaRepository<Account, Integer> {

}
