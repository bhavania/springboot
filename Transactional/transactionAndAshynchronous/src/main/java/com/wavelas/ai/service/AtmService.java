package com.wavelas.ai.service;

import java.util.ArrayList;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelas.ai.model.Account;
import com.wavelas.ai.repository.AtmRepository;

@Service
public class AtmService {
	List<Account> acc=new ArrayList<Account>();
	@Autowired
	private AtmRepository atmrepository;

	public Account deposit(Account atm) {
		return atmrepository.save(atm);

	}

	public List<Account> showBalance() {
		acc= atmrepository.findAll();
		return acc;
	}
@Transactional(rollbackOn = Exception.class)
	public Account updateAccountBalan(Account atm, double withDrawAmont)
	{
		double amount=atm.getBalance()-withDrawAmont;
		atm.setBalance(amount);
		return atmrepository.saveAndFlush(atm);

	}

}
