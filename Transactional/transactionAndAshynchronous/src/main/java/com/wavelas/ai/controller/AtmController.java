package com.wavelas.ai.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelas.ai.model.Account;
import com.wavelas.ai.service.AtmService;

@RestController
public class AtmController {
	List<Account> li = new ArrayList<Account>();
	@Autowired
	private AtmService atmservice;

	@GetMapping(value = "/atms")
	public ResponseEntity<List<Account>> getAtmDetails() {
		li = atmservice.showBalance();
		return ResponseEntity.status(200).body(li);
	}

	@PutMapping("/atms/{accountnumber}")
	public ResponseEntity<Account> updateAccountBalances(@RequestBody Account atm,
			@PathVariable("accountnumber") Integer accountnumber) {
		for(Account acc:li)
		{
			if(acc.getAccountnumber()==accountnumber)
			 atmservice.updateAccountBalan(atm, accountnumber);
		}
		
		return ResponseEntity.status(200).body(atm);
	}

	@PostMapping("/atms1")
	public ResponseEntity<Account> saveAtm(@RequestBody Account atm) {
		Account atm1 = atmservice.deposit(atm);
		return ResponseEntity.status(200).body(atm1);
	}
}