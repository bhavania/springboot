package com.wavelabs.ai;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.ai.model.Post;
import com.wavelabs.ai.model.Tag;
import com.wavelabs.ai.repository.PostRepository;
import com.wavelabs.ai.repository.TagRepository;

@SpringBootApplication
public class ManytomanyApplication implements CommandLineRunner {

    @Autowired
    private TagRepository tagRepositoory;

    @Autowired
    private PostRepository postRepository;

    public static void main(String[] args) {
        SpringApplication.run(ManytomanyApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        postRepository.deleteAllInBatch();
        JpaRepository<Post, Long> tagRepository;
		tagRepository.deleteAllInBatch();
        Post post = new Post("Hibernate Many to Many Example with Spring Boot",
                "Learn how to map a many to many relationship using hibernate",
                "Entire Post content with Sample code");

        // Create two tags
        Tag tag1 = new Tag("Spring Boot");
        Tag tag2 = new Tag("Hibernate");


        // Add tag references in the post
        ((Object) post.getTags()).add(tag1);
        ((Object) post.getTags()).add(tag2);

        // Add post reference in the tags
        ((Object) tag1.getPosts()).add(post);
        tag2.getPosts().add(post);

        postRepository.save(post);

       

    }
}