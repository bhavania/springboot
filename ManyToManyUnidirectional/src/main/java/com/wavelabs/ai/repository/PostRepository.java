package com.wavelabs.ai.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

}