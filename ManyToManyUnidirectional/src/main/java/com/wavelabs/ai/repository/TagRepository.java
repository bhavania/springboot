package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

}