package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Project;
import com.wavelabs.ai.repository.ProjectRepository;

@Service
public class ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	public Project saveProject(Project project) {
		return projectRepository.save(project);

	}

	public List<Project> getAllProjects() {
		return projectRepository.findAll();

	}

	public Project updateProject(Integer id,Project project)
	{
		return projectRepository.saveAndFlush(project);
	}

}
