package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.ai.model.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{


}
