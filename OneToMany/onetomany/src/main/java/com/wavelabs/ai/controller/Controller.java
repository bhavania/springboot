package com.wavelabs.ai.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Project;
import com.wavelabs.ai.service.ProjectService;

@RestController
public class Controller {

	@Autowired
	private ProjectService projectService;

	@PostMapping(value = "/projects", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Project> saveAllProject(@RequestBody Project project) {
		Project project1 = projectService.saveProject(project);
		return ResponseEntity.status(201).body(project1);
	}

	@GetMapping(value = "/projects", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Project>> getAllProject() {
		List<Project> project = projectService.getAllProjects();
		return ResponseEntity.status(201).body(project);

	}
	@PutMapping(value="/projects/{id}")
    public ResponseEntity<Project> updateTicket(@PathVariable("id") Integer id, @RequestBody Project project) {
	Project project1=projectService.updateProject(id,project);
		return ResponseEntity.status(200).body(project1);
	}
}