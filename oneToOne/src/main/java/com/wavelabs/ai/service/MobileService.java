package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Mobile;
import com.wavelabs.ai.repository.MobileRepository;
@Service
public class MobileService {

	@Autowired
	private MobileRepository mobileRepository;

	public Mobile saveMobile(Mobile mobile) {
		return mobileRepository.save(mobile);

	}

	public List<Mobile> getAllMobile() {
		return mobileRepository.findAll();

	}

	public Mobile updateMobile(Integer id,Mobile mobile)
	{
		return mobileRepository.saveAndFlush(mobile);
	}
}
