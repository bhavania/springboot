package com.wavelabs.ai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Mobile;
import com.wavelabs.ai.service.MobileService;

@RestController
public class MobileController {

	@Autowired
	private MobileService mobileService;

	@PostMapping(value = "/mobils", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Mobile> saveAllMobile(@RequestBody Mobile mobile) {
		Mobile mobile1 = mobileService.saveMobile(mobile);
		return ResponseEntity.status(201).body(mobile1);
	}

	@GetMapping(value = "/mobils", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Mobile>> getAllMobileDetails() {
		List<Mobile> mobile = mobileService.getAllMobile();
		return ResponseEntity.status(201).body(mobile);

	}

	@PutMapping(value = "/mobils/{id}")
	public ResponseEntity<Mobile> updateMobile(@PathVariable("id") Integer id, @RequestBody Mobile mobile) {
		Mobile mobile1 = mobileService.updateMobile(id, mobile);
		return ResponseEntity.status(200).body(mobile1);
	}
}
