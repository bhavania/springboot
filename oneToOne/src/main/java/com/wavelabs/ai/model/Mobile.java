package com.wavelabs.ai.model;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mobile1")
public class Mobile {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	String name;


public Mobile()
{
	
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
	
@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
private Ip ip;

}
