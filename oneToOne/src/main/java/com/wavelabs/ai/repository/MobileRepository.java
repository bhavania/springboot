package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Mobile;
@Repository
public interface MobileRepository extends JpaRepository<Mobile, Integer>{

}
