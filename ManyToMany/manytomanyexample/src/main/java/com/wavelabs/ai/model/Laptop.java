package com.wavelabs.ai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LAPTOP")
public class Laptop {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int lapid;
	@Column
	String name;

	public Laptop(int lapid, String name) {
		super();
		this.lapid = lapid;
		this.name = name;
	}

	public Laptop() {

	}

	public int getLapid() {
		return lapid;
	}

	public void setLapid(int lapid) {
		this.lapid = lapid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}