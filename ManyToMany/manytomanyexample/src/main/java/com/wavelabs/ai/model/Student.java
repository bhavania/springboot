package com.wavelabs.ai.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "student123")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int sid;
	@Column
	String name;
	@Column
	String email;
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Laptop> laptop = new ArrayList<>();

	public Student(int sid, String name, String email) {
		super();
		this.sid = sid;
		this.name = name;
		this.email = email;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Laptop> getLaptop() {
		return laptop;
	}

	public void setLaptop(List<Laptop> laptop) {
		this.laptop = laptop;
	}

	public Student() {

	}

}