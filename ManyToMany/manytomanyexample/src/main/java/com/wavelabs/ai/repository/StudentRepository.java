package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>
{
}
