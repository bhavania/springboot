package com.wavelabs.ai.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Movie;
import com.wavelabs.ai.model.SuperHero;
import com.wavelabs.ai.service.RelationService;


@RestController
public class RelationsController {
	
	@Autowired
	RelationService relationService;
	
	
	@RequestMapping(value = "/movies", method = RequestMethod.POST)
	public ResponseEntity<?> postMovie(@RequestBody Movie movie){
		relationService.saveMovie(movie);
		return ResponseEntity.status(200).body("Movie created");
	}
	
	@RequestMapping(value = "/superheros", method = RequestMethod.POST)
	public ResponseEntity<?> postSuperHeros(@RequestBody SuperHero hero){
		relationService.saveSuperHero(hero);
		return ResponseEntity.status(200).body("Hero created");
	}
	
	

}