package com.wavelabs.ai.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Movie;
import com.wavelabs.ai.model.SuperHero;
import com.wavelabs.ai.repository.MovieRepository;
import com.wavelabs.ai.repository.SuperHeroRepository;
@Service
public class RelationService {
	
	@Autowired
	MovieRepository movieRepository;
	
	@Autowired
	SuperHeroRepository superHeroRepository;

	public void saveMovie(Movie movie) {
		movieRepository.save(movie);
	}

	public void saveSuperHero(SuperHero superHeroes) {
		superHeroRepository.save(superHeroes);
	}

}