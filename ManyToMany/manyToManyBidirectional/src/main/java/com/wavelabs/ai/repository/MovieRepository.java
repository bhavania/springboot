package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>{

}