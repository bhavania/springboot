package com.wavelabs.ai.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.SuperHero;

@Repository
public interface SuperHeroRepository extends JpaRepository<SuperHero, Integer>{

}