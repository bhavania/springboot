package com.wavelabs.ai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class RetryRecoverable1Application {

	public static void main(String[] args) {
		SpringApplication.run(RetryRecoverable1Application.class, args);
	}

}
