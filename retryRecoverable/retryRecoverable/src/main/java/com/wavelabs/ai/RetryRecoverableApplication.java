package com.wavelabs.ai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@SpringBootApplication
@RestController
@EnableRetry
public class RetryRecoverableApplication {
	public static void main(String[] args) {
		SpringApplication.run(RetryRecoverableApplication.class, args);
	}
	
	@GetMapping(value="/test")
	@Retryable(value={NullPointerException.class,NumberFormatException.class  },maxAttempts = 3)
	public String myApp()
	{
		System.out.println("retry started");
		return "Retry is working";
	}
	
	
	@Recover
	public String recover(NullPointerException e)
	{
		System.out.println("recoverable is started-NullPointerException");
		return "recoverable is working";
	}
	@Recover
	public String recover(NumberFormatException e)
	{
		System.out.println("recoverable is started-NumberFormatException");
		return "recoverable is working second";
	}

}
