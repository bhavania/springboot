package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Hotel;
import com.wavelabs.ai.repository.HotelRepository;

@Service
public class HotelService {
	@Autowired
	private HotelRepository hotelRepository;

	public Hotel saveHotel(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public List<Hotel> GetAllHotelDetails() {
		return hotelRepository.findAll();
	}

	public Hotel updateHotel(Integer id, Hotel hotel) {
		return hotelRepository.saveAndFlush(hotel);
	}
}
