package com.wavelabs.ai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.ai.model.Hotel;
import com.wavelabs.ai.model.Order;
import com.wavelabs.ai.service.HotelService;

@RestController
public class HotelController {
	@Autowired
	 RestTemplate restTempalte;
	
	@RequestMapping("/hotels")
	public ResponseEntity<Order> getMap() {
		ResponseEntity<Order> response = restTempalte.getForEntity("http://localhost:8080/orders", Order.class);
		return response;
	}
	
	@Autowired
	private HotelService hotelService;

	@GetMapping(value = "/hotels")
	public ResponseEntity<List<Hotel>> GetAllHotelDetails() {
		List<Hotel> hotel = hotelService.GetAllHotelDetails();

		return ResponseEntity.status(200).body(hotel);
	}

	@PostMapping(value = "/hotels")
	public ResponseEntity<Hotel> saveHotel(@RequestBody Hotel hotel) {
		Hotel hotel1 = hotelService.saveHotel(hotel);
		return ResponseEntity.status(200).body(hotel1);
	}

	@DeleteMapping(value = "/hotels/{id}")
	public ResponseEntity<Hotel> updateHotels(@PathVariable("id") Integer id, @RequestBody Hotel hotel) {
		Hotel hotel1 = hotelService.updateHotel(id, hotel);
		return ResponseEntity.status(200).body(hotel1);
	}

	@PutMapping(value = "/hotels/{id}")
	public ResponseEntity<Hotel> updateHotel(@PathVariable("id") Integer id, @RequestBody Hotel hotel) {
		Hotel hotel1 = hotelService.updateHotel(id, hotel);
		return ResponseEntity.status(200).body(hotel1);
	}

}
