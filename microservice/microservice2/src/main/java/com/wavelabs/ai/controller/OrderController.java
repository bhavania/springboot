package com.wavelabs.ai.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Order;
import com.wavelabs.ai.service.OrderService;


@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;

	@GetMapping(value = "/orders")
	public ResponseEntity<List<Order>> GetAllOrderDetails() {
		List<Order> order = orderService.GetAllOrderDetails();

		return ResponseEntity.status(200).body(order);
	}

	@PostMapping(value = "/orders")
	public ResponseEntity<Order> saveOrder(@RequestBody Order order) {
		Order order1 = orderService.saveOrder(order);
		return ResponseEntity.status(200).body(order1);
	}

	@DeleteMapping(value = "/orders/{id}")
	public ResponseEntity<Order> updateOrders(@PathVariable("id") Integer id, @RequestBody Order order) {
		Order order1 = orderService.updateOrder(id, order);
		return ResponseEntity.status(200).body(order1);
	}

	@PutMapping(value = "/Orders/{id}")
	public ResponseEntity<Order> updateOrder(@PathVariable("id") Integer id, @RequestBody Order order) {
		Order order1 = orderService.updateOrder(id, order);
		return ResponseEntity.status(200).body(order1);
	}

}

