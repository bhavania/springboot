package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

	
}
