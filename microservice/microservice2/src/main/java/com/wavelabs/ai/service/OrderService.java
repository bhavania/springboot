package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Order;
import com.wavelabs.ai.repository.OrderRepository;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;

	public Order saveOrder(Order order) {
		return orderRepository.save(order);
	}

	public List<Order> GetAllOrderDetails() {
		return orderRepository.findAll();
	}

	public Order updateOrder(Integer id, Order order) {
		return orderRepository.saveAndFlush(order);
	}
 
}
