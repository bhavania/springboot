package com.wavelabs.ai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ai.model.Bank;
@Repository
public interface BankRepository extends JpaRepository<Bank, Integer>{

}
