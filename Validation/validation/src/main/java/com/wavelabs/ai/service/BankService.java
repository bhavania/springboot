package com.wavelabs.ai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.ai.model.Bank;
import com.wavelabs.ai.repository.BankRepository;
@Service
public class BankService {
@Autowired
private BankRepository bankRepository;

public Bank saveBankDetails(Bank bank)
{
	return bankRepository.save(bank);
}
public List<Bank> GetBankDetails()
{
	return bankRepository.findAll();
}

}
