package com.wavelabs.ai.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ai.model.Bank;
import com.wavelabs.ai.service.BankService;

@RestController
public class BankController {
	@Autowired
	private BankService bankService;

@GetMapping(value="/banks")
public ResponseEntity<List<Bank>> GetAllBankDetails()
{
	List<Bank> bank=bankService.GetBankDetails();
	return ResponseEntity.status(200).body(bank);
}

@PostMapping(value="/banks")
public ResponseEntity<Bank>saveAllBankDetails(@Valid @RequestBody Bank bank)
{
	Bank bank1= bankService.saveBankDetails(bank);
	return ResponseEntity.status(200).body(bank1);
}
}
